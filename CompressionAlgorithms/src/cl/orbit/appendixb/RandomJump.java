/**
 * 
 */
package cl.orbit.appendixb;

/**
 * @author sysop
 *
 */
public class RandomJump {

	
	private int valor;
	private int mismoRango;
	private int rango; //0 1 2
	int sample;
	
	public RandomJump ()
	{
		sample = 0;
		refresh();
	}
	
	private void refresh ()
	{
		mismoRango = 5 + ( (int) (Math.random() * 20)) ;
		rango = ( (int) (Math.random() * 1000)) % 3;
	}
	
	private int randomEnRango(int rango) {
		switch (rango) {
		case 0:
			return (( (int) ((Math.random() * 127 ))) - 127);
		
		case 1:
			return (( (int) ((Math.random() * 32767 ))) - 32767);
		case 2:
			return (( (int) ((Math.random() * 2147483646 ))) - 2147483646);
		}
		return 0;
	}
	
	public int next()
	{
		mismoRango--;
		if (mismoRango<0) {
			this.refresh();
		}
		sample +=randomEnRango(rango);
		return sample;
	}
	
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		RandomJump rj;
		rj = new RandomJump();
		int aleatorio;
		
		for (int i=0;i<300;i++)
		{
			aleatorio = rj.next();
			System.out.println("aleatorio="+aleatorio);
		}

	}

}

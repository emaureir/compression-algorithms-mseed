package cl.orbit.appendixb;

public class SensorEvent {

	public long timestamp;
	public float[] values;

	public SensorEvent(long lastTimestamp, int valor)
	{
		this.timestamp = lastTimestamp + (long) (Math.random()*1000);
		values = new float[3];
		for (int i=0;i<3;i++)
		{
			//values[i]= (float) Math.random();
			values[i]= (float) (valor * Math.sin( valor / 360));//(float) Math.pow((((float) (this.timestamp))/25000), 2);
		}
	}

	public SensorEvent(long timestamp, int[] intValues) {
		super();
		this.timestamp = timestamp;
		this.values = new float[3];
		for (int i=0;i<3;i++)
		{
			this.values[i] = Float.intBitsToFloat(intValues[i]);
		}
	}

	public SensorEvent(long timestamp, float[] values) {
		super();
		this.timestamp = timestamp;
		this.values = values;
	}
	
	
	
	
}

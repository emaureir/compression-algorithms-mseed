package cl.orbit.appendixb;

/**
 * Forma economica de representar un número entero (unsigned int) de 4 bytes.
 * En la lógica del complemento de 2,
 * Los enteros de menor valor absoluto son representables con menos bits.
 * 
 *  Para efectos de Steim1, aqui se aprovecha de representar enteros ya
 *  sea con 2 , 4 u 8 bytes segun sea posible esta representación.
 *  
 *  1 Bytes--> 							[-127, 127]
 *  2 Bytes--> 				[-32767, -128]  	U 	[128, 32767]
 *  4 Bytes--> [-2147483648, -32768] 		U 			[32768, 2147483646]
 *  
 * En resumen, esta clase permite representar un entero de manera economica
 * y ademas decodificar la manera economica de representarlo.
 * 
 * Se implementa esto para aprovechar Steim1 en la etapa de comunicaciones
 * TCP/IP de datos sismológicos.
 *  
 * @author sysop
 * @see refreshMinSteim1Bytes()
 */
public class EcoInt {

	/**
	 * barray[0] es el Byte mas significativo
	 */
	private byte[] barray;

	/**
	 * El entero que se está representando economicamente
	 */
	private int entero;	

	/**
	 * Cantidad mínima de bytes necesarios para representar el entero. 
	 * 1, 2 o 4
	 */
	private short minSteim1Bytes;

	/**
	 * Al crear este objeto ademas se descompondra el entero ingresado en 
	 * sus 4 bytes y se calculará el minimo de bytes necesarios para representarlo.
	 * @param extendedInt Numero Entero que la maquina virtual de Java representa con 4 bytes.
	 */
	public EcoInt(int extendedInt)
	{
		this.entero = extendedInt;
		//barray = new byte[4];
		barray = Utiles.IntToByteArray(extendedInt, (short)4); 

		// A continuación se descompone el número entero en sus 4 bytes
		//		short shift;
		//		for (short i=0;i<4;i++)
		//		{
		//			shift = (short) ((3-i)*8);
		//			barray[i] = (byte)((this.entero & (0xff<<shift)) >> shift);
		//		}

		//		barray[0] = (byte)((this.entero & 0xff000000) >> 24);
		//		barray[1] = (byte)((this.entero & 0x00ff0000) >> 16);
		//		barray[2] = (byte)((this.entero & 0x0000ff00) >> 8);
		//		barray[3] = (byte)((this.entero & 0x000000ff));
		this.refreshMinSteim1Bytes();
	}

	/**
	 * Calcula la cantidad de bytes necesarios para la representacion eficiente
	 * del entero en compresion Steim1
	 * @return
	 */
	public void  refreshMinSteim1Bytes()
	{
		short leadingZeros;
		short preSteim;
		if ((this.entero)<0)
		{ 
			leadingZeros = (short)(32 - Integer.numberOfLeadingZeros(-(this.entero)));
		} else
		{
			leadingZeros = (short)(32 - Integer.numberOfLeadingZeros((this.entero)));
		}
		// Si se necesitan [16,32]	bits --> 4 bytes
		// Si se necesitan [8,15] 	bits --> 2 bytes
		// Si se necesitan [0,7] 	bits --> 1 bytes
		if (leadingZeros > 15)			{	preSteim = 4;}
		else {
			if (leadingZeros > 7)		{	preSteim = 2;}
			else 						{	preSteim = 1;}
		}
		this.minSteim1Bytes = preSteim;
	}

	/**
	 * Codifica el numero entero en la representacion Steim1
	 * segun la cantidad minima de bytes.
	 * Este metodo copia los bytes de barray al arrego retornado en el orden
	 * del byte menos significativo al más significativo.
	 * Pero solo copia y retorna un arreglo del tamaño eficiente para representar el entero.
	 * @return Un arreglo del tamaño Steim1 eficiente para representar el entero.
	 */
	public byte[] getByteRepresentation()
	{		
		byte respuesta[] = new byte[this.minSteim1Bytes];
		for (short i=0;i<this.minSteim1Bytes;i++)
		{
			respuesta[i] = barray[(4-this.minSteim1Bytes)+i];
		}
		return respuesta;
	}

	/**
	 * Por ejemplo, Un nibble de 2 bytes podria estar albergando un entero de 1Byte
	 * y uno de 2 Bytes.
	 * En este caso, el entero de 1Byte debe ser representado en 2 Bytes usando este metodo 
	 * @param arrayLength Largo (en bytes) del arreglo solicitado.
	 * @return null si el arreglo no se puede representar con esa cantidad de bytes.
	 */
	public byte[] getByteRepresentation(short arrayLength)
	{
		if (this.minSteim1Bytes>arrayLength)
		{
			System.out.println("nibbleType="+arrayLength+" this.minSteim1Bytes="+this.minSteim1Bytes);
			return null;

		}

		byte respuesta[] = new byte[arrayLength];
		for (short i=0;i<arrayLength;i++)
		{
			respuesta[i] = barray[(4-arrayLength)+i];
		}
		return respuesta;		
	}

	@Override
	/**
	 * Muestra los 4 bytes en que se descompuso el entero original
	 * en orden de mas a menos significativo (de izq a derecha).
	 */
	public String toString() {
		StringBuilder respuesta;
		respuesta = new StringBuilder();
		for (short i=0;i<4;i++)
		{
			respuesta.append( " /// "+barray[i]);
		}
		respuesta.append("\n");
		return respuesta.toString();		
	}

	public byte[] getBarray() {
		return barray;
	}

	public void setBarray(byte[] barray) {
		this.barray = barray;
	}

	public int getEntero() {
		return entero;
	}

	public void setEntero(int entero) {
		this.entero = entero;
	}

	public short getMinSteim1Bytes() {
		return minSteim1Bytes;
	}

	public void setMinSteim1Bytes(short minSteim1Bytes) {
		this.minSteim1Bytes = minSteim1Bytes;
	}

	public static void main(String[] args) {
		int test;
		test = 2;
		switch (test) {
		case 1:
			test1();
			break;
		case 2:
			test2();
			break;
		case 3:
			test3();
			break;

		default:
			test1();
			break;
		}
	}

	/**
	 * Revisa que para un entero, la cantidad minima de bytes requeridos
	 * para Steim1 sea correcta
	 */
	private static void test1() {
		System.out.println("Test 1");
		EcoInt ib;
		//for (int i=Integer.MAX_VALUE-10;i<Integer.MAX_VALUE;i++)
		//for (int i=Integer.MIN_VALUE;i<Integer.MIN_VALUE+10;i++)
		//for (int i=-32770;i<-32760;i++)
		//		for (int i=-130;i<-120;i++)
		//for (int i=-130;i<130;i++)
		for (int i=32760;i<32770;i++)
		{
			ib =  new EcoInt(i);
			System.out.println("minRequiredBytes("+i+")="+ib.minSteim1Bytes);
		}
	}

	/**
	 * Muestra la codificacion Steim1 para la lista de
	 * enteros definida en el loop
	 */
	private static void test2() {
		EcoInt ib;
		byte[] repres;
		int largo, j;
		//for (int i=Integer.MAX_VALUE-10;i<Integer.MAX_VALUE;i++)
		//for (int i=Integer.MIN_VALUE;i<Integer.MIN_VALUE+10;i++)
			//for (int i=-32770;i<-32760;i++)
			//		for (int i=-130;i<-120;i++)
			for (int i=-130;i<130;i++)
			//			for (int i=32760;i<32770;i++)
		{
			ib =  new EcoInt(i);
			System.out.print(i+"-->");
			repres = ib.getByteRepresentation();
			largo = repres.length;
			for (j=0;j<largo;j++)
			{
				System.out.print(repres[j]+"##");
			}
			System.out.println("__");
		}
	}

	/**
	 * Comprueba que todos los valores representables por
	 * int sean correctamente codificables y decodificables en Steim1
	 */
	private static void test3() {
		System.out.println("test 3");
		EcoInt ib;
		int enteroDecodificado;
		int cantErrores;
		cantErrores= 0;
		//for (int i=0;i<Integer.MAX_VALUE;i++)
		//for (int i=-32770;i<32770;i++)
		for (int i=Integer.MIN_VALUE;i<Integer.MAX_VALUE;i++)
		{
			ib =  new EcoInt(i);
			enteroDecodificado = Utiles.decodeByteRepresentation(ib.getByteRepresentation((short)4));
			//enteroDecodificado = byteToInt(ib.getByteRepresentation());
			if ((i%10000000)==0)
			{
				System.out.println("revisando "+i);
			}
			if (i!=enteroDecodificado)
			{
				cantErrores++;
				System.out.println("Error en el entero"+i+" != "+enteroDecodificado+" -->"+ib.toString()+" reqBytes="+ib.minSteim1Bytes);
			}
		}
		System.out.println("Test finalizado, cantErrores="+cantErrores);
	}
}

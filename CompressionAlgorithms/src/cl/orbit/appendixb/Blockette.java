/**
 * 
 */
package cl.orbit.appendixb;

/**
 * @author sysop
 *
 */
public abstract class Blockette {

	protected byte[] blocketteCode;
	protected byte[] offSet;

	public static final byte[] BLOCKETTE1000 = { (byte) 0x3 , (byte) 0xE8 };
	public static final byte[] BLOCKETTE1001 = { (byte) 0x3 , (byte) 0xE9 };

	
	
	public Blockette(byte[] blocketteCode, byte[] offSet) {
		super();
		this.blocketteCode = blocketteCode;
		this.offSet = offSet;
	}


	public byte[] getBlocketteCode() {
		return blocketteCode;
	}


	public void setBlocketteCode(byte[] blocketteCode) {
		this.blocketteCode = blocketteCode;
	}


	public byte[] getOffSet() {
		return offSet;
	}


	public void setOffSet(byte[] offSet) {
		this.offSet = offSet;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

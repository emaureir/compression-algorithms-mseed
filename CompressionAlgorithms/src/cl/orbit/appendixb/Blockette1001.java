/**
 * 
 */
package cl.orbit.appendixb;

/**
 * @author sysop
 *
 */
public class Blockette1001 extends Blockette {

	/**
	 * Timing quality. Can be used by the digitizer manufacturer to extimate the time quality of this data packet form 0 to 100% of accuracy.
	 */
	private byte timingQuality;
	/**
	 * Precision of the start time down to microseconds. This filed is present to improve the accuracy of the time stamping 
	 * given by the fixed header time structure.
	 */
	private byte microseconds;
	private byte reserved;
	/**
	 * Frame count. Is the number of 64 byte compressed data frames in the 4k record. (maximum of 63).
	 */
	private byte frameCount;
	
	public Blockette1001 (byte[] offSet, byte timingQuality, byte frameCount)
	{
		super(Blockette.BLOCKETTE1001, offSet);
		this.timingQuality = timingQuality;
		this.microseconds =(byte)0;
		this.reserved = (byte)70;
		this.frameCount = frameCount;
		
	}
	
	public byte[] toByteArray()
	{
		byte[] respuesta;
		respuesta  = new byte[8];
		respuesta[0] =blocketteCode[0];
		respuesta[1] =blocketteCode[1];
		respuesta[2] =offSet[0];
		respuesta[3] =offSet[1];
		respuesta[4] = timingQuality;
		respuesta[5] = microseconds;
		respuesta[6] = reserved;
		respuesta[7] = frameCount;		
		return respuesta;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

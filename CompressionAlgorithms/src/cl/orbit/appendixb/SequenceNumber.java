/**
 * 
 */
package cl.orbit.appendixb;

import java.text.DecimalFormat;

/**
 * @author sysop
 *
 */
public class SequenceNumber {

	private int indice;
	public static final DecimalFormat myFormat = new DecimalFormat("000000");
	
	
	
	public int getIndice() {
		return indice;
	}

	public void setIndice(int indice) {
		this.indice = indice;
	}
	
	

	@Override
	public String toString() {
		byte[] respuesta;
		respuesta = toByteArray();
		StringBuilder resp;
		resp = new StringBuilder();
		for (int i=0;i<respuesta.length;i++)
		{
			resp.append((char) respuesta[i]);
		}
		return resp.toString();
	}

	public byte[] toByteArray ()
	{
		byte[] respuesta;
		char[] chrRespuesta;
		respuesta = new byte[6];		
		chrRespuesta = myFormat.format(indice).toCharArray();
		for (int i=0;i<6;i++) respuesta[i] = (byte) chrRespuesta[i];
		return respuesta;
	}
	            
	public void next ()
	{	
		this.indice++;
	}
	
	public SequenceNumber()
	{
		indice = 0;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SequenceNumber sn;
		sn = new SequenceNumber();
		for (int i=0;i<10;i++)
		{
			System.out.println(sn);
			sn.next();
		}
	}

}

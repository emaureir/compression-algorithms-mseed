package cl.orbit.appendixb;

import java.util.Vector;



public class StaLta {

	private Vector<SensorEvent> vSamples;
	private SensorEvent lastSample;

	private long newestTimeStamp;

	private long maxShortTermSpan;
	private long maxLongTermSpan;

	/**
	 * Representa la ventana de tiempo que de acuerdo al arreglo de samples cumple con el rango predefinido
	 * Se actualiza con checkLongTermSpan.
	 */
	private long spanLT;

	/**
	 * Representa la ventana de tiempo que de acuerdo al arreglo de samples cumple con el rango predefinido
	 * Se actualiza con checkShortTermSpan.
	 */
	private long spanST;

	/**
	 *  Index Short Term
	 */
	private int ist;

	/**
	 * Index Long Term
	 */
	private int ilt; 

	private float[] sta;
	private float[] lta;

	private float staSum[];
	private float ltaSum[];

	/**
	 * 
	 * @param maxShortTermSpan en Segundos
	 * @param maxLongTermSpan en Segundos
	 */
	public StaLta (double maxShortTermSpan, double maxLongTermSpan)
	{
		this.maxLongTermSpan = (long) (maxLongTermSpan * 10000000000L);
		this.maxShortTermSpan = (long) (maxShortTermSpan * 10000000000L);
		this.vSamples = new Vector<SensorEvent>();
		newestTimeStamp = -1;
		ist = ilt = 0;
		staSum  = new float[3];
		ltaSum  = new float[3];
		sta = new float[3];
		lta = new float[3];

		for (int i=0;i<3;i++)
		{
			staSum[i]=ltaSum[i]=0;
			maxSta[i] = Float.MIN_VALUE;
			maxLta[i] = Float.MIN_VALUE;
		}
	}

	public void addSample(SensorEvent evt){

		lastSample = evt;
		vSamples.add(lastSample);						

		for(int i=0;i<3;i++)
		{
			ltaSum[i]+= Math.abs(lastSample.values[i]);
			staSum[i]+= Math.abs(lastSample.values[i]);
		}
		newestTimeStamp = lastSample.timestamp;
		this.checkLongTermSpan();
		this.checkShortTermSpan();
		this.refreshStaLta();
		if (vSamples.size()>160000)
		{
			vSamples.remove(0);
			ist--;
			ilt--;
		}
	}

	private void CompruebaSumas ()
	{
		float[] lstaSum;
		float[] lltaSum;
		lstaSum = new float[3];
		lltaSum = new float[3];
		int largoVsamples;
		largoVsamples = vSamples.size();
		for (int i=0;i<3;i++)
		{
			lstaSum[i] = lltaSum[i]=0;
		}
		for (int index=ist;index<largoVsamples;index++)
		{
			for (int i=0;i<3;i++)
			{
				lstaSum[i] += Math.abs( vSamples.elementAt(index).values[i]);
			}
		}
		for (int index=ilt;index<largoVsamples;index++)
		{
			for (int i=0;i<3;i++)
			{
				lltaSum[i] +=  Math.abs( vSamples.elementAt(index).values[i]);
			}
		}
		for (int i=0;i<3;i++)
		{			
			//			System.out.print("(lstaSum["+i+"]==staSum["+i+"])="+(lstaSum[i]==staSum[i]));;
			//			System.out.println("\t\t(lltaSum["+i+"]==ltaSum["+i+"])="+(lltaSum[i]==ltaSum[i]));;
			System.out.print(lstaSum[i]+"<->"+staSum[i]);
			System.out.println("\t\t"+lltaSum[i]+"<->"+ltaSum[i]);
		}




	}



	/**
	 * actualiza ilt, spanLT
	 *  de modo que el spanLT este en el rango predefinido
	 *  mientras actualiza, ademas ajusta los valores del arreglo ltaSum
	 */
	private void checkLongTermSpan()
	{
		long timeStampLT;


		timeStampLT = vSamples.elementAt(ilt).timestamp;
		spanLT = this.newestTimeStamp - timeStampLT;
		// Primero evaluamos la posibilidad de dejar intacto ilt
		if (spanLT<maxLongTermSpan)
		{
			return;
		}
		// Avanzamos en el tiempo hasta que la ventana de LongTerm sea menor que el
		// Maximo definido
		while (spanLT>=maxLongTermSpan)
		{
			for(int i=0;i<3;i++)
			{
				ltaSum[i]-=  Math.abs(vSamples.elementAt(ilt).values[i]);
			}
			ilt++;
			timeStampLT = vSamples.elementAt(ilt).timestamp;
			spanLT = this.newestTimeStamp - timeStampLT;
		}
	}


	/**
	 * actualiza ilt, spanLT
	 *  de modo que el spanLT este en el rango predefinido
	 *  mientras actualiza, ademas ajusta los valores del arreglo ltaSum
	 */
	private void checkShortTermSpan()
	{
		long timeStampST;


		timeStampST = vSamples.elementAt(ist).timestamp;
		spanST = this.newestTimeStamp - timeStampST;
		// Primero evaluamos la posibilidad de dejar intacto ilt
		if (spanST<maxShortTermSpan)
		{
			return;
		}
		// Avanzamos en el tiempo hasta que la ventana de LongTerm sea menor que el
		// Maximo definido
		while (spanST>=maxShortTermSpan)
		{
			for(int i=0;i<3;i++)
			{
				staSum[i]-=  Math.abs(vSamples.elementAt(ist).values[i]);
			}
			ist++;
			timeStampST = vSamples.elementAt(ist).timestamp;
			spanST = this.newestTimeStamp - timeStampST;
		}
	}

	private float[] maxSta = new float[3];
	private float[] maxLta = new float[3];

	private void refreshStaLta()
	{
		for ( int i=0;i<3;i++) {
			if (spanST>0){
				sta[i] =  staSum[i]/spanST;
			}
			if (spanLT>0){
				lta[i] =  ltaSum[i]/spanLT;
			}
			if (maxSta[i]<sta[i])  maxSta[i]=sta[i];
			if (maxLta[i]<lta[i])  maxLta[i]=lta[i];
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder respuesta;
		respuesta = new StringBuilder();
		//		respuesta.append("#="+this.vSamples.size());
		//		respuesta.append(" /// ShortTerm:["+ist+"]("+spanST+"<"+maxShortTermSpan+")="+(spanST<maxShortTermSpan));
		//		respuesta.append(" /// LongTerm:["+ilt+"]("+spanLT+"<"+maxLongTermSpan+")="+(spanLT<maxLongTermSpan));
		//
		//		respuesta.append("\n sta = ");
		//		for ( int i=0;i<3;i++) {
		//			respuesta.append( (sta[i]*100000000) );
		//			respuesta.append("\t");
		//		}
		//		respuesta.append("\n lta = ");
		//		for ( int i=0;i<3;i++) {
		//			respuesta.append( (lta[i]*100000000) );
		//			respuesta.append("\t");
		//		}
		//		respuesta.append("\n");

		boolean cond = false;
		for ( int i=0;i<3;i++) {
			if (( sta[i]/lta[i])>1.4)
			{
				respuesta.append( "sta/lta ["+i+"]= "+( sta[i]/lta[i]) );
				respuesta.append("\t");
				cond = true;
			}
		}
		if (cond)
			respuesta.append("\n");
		//		for ( int i=0;i<3;i++) {
		//			respuesta.append( "maxSta ["+i+"]= "+( maxSta[i]) );
		//			respuesta.append("\t");
		//		}
		//		respuesta.append("\n");
		//		for ( int i=0;i<3;i++) {
		//			respuesta.append( "maxLta ["+i+"]= "+( maxLta[i]) );
		//			respuesta.append("\t");
		//		}
		//		respuesta.append("\n");


		return respuesta.toString();
	}

	public static void main(String[] args) {
		test1();
	}

	private static void test1() {
		int cantEventos = 1000;
		long lastTimeStamp = 0;
		SensorEvent evt;
		StaLta deteccion;
		deteccion = new StaLta(5000, 6000);
		for (int i=0;i<cantEventos;i++)
		{
			evt = new SensorEvent(lastTimeStamp,i);
			deteccion.addSample(evt);
			//System.out.println(deteccion.toString());
			deteccion.CompruebaSumas();
			lastTimeStamp = evt.timestamp;
			//			if ((i%100)==0)
			//			{
			//				try {
			//					Thread.sleep(990);
			//				} catch (InterruptedException e) {
			//					System.err.println("Error Sleep.");
			//					e.printStackTrace();
			//				}
			//			}
		}		
	}

}

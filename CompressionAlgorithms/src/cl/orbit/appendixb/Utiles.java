/**
 * 
 */
package cl.orbit.appendixb;

/**
 * @author sysop
 *
 */
public class Utiles {

	public static byte[] IntToByteArray (int entero, short size)
	{
		byte[] respuesta = new byte[size];
		// A continuación se descompone el número entero en sus "size" bytes
		short shift;
		for (short i=0;i<size;i++)
		{
			shift = (short) (((size-1)-i)*8);
			respuesta[i] = (byte)((entero & (0xff<<shift)) >> shift);
		}
		return respuesta;
	}
	
	/**
	 * Despliega un arreglo de bytes en una forma de lectura comoda
	 * @param byteArray
	 * @return
	 */
	public static String toString(byte[] byteArray)
	{
		StringBuilder sb;
		sb = new StringBuilder();
		int largo;
		largo = byteArray.length;
		for (int i=0;i<largo;i++)
		{
			sb.append("  "+ Integer.toHexString(byteArray[i]).toUpperCase());
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param texto Texto a convertir en arreglo de bytes
	 * @param largo largo del arreglo de bytes requerido
	 * @return
	 */
	public static byte[] PadLeft (String texto, int largo)
	{
		char[] chrText;
		byte[] respuesta;
		int largoTXT;
		largoTXT = texto.length();
		respuesta = new byte[largo];
		// Si el largo del arreglo solicitado supera al largo del texto ingresado
		// Se blanquea con espacios el arreglo a retornar
		if (largo> largoTXT)
			for (int i=texto.length();i<largo;i++)
			{
				respuesta[i] = (' ');
			}

		chrText = texto.toCharArray();
		for (int i=0;i<Math.min(largo, largoTXT);i++)
		{
			respuesta[i] = (byte) chrText[i];
		}	
		return respuesta;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//testPadLeft();
		testFloat ();
	}
	
	/**
	 * Evalua la factibilidad de crear un mapa 1:1 entre int<--->float.
	 * usando el metodo intBitsToFloat
	 */
	public static void testFloat ()
	{
		
		for (int i=0;i<Integer.MAX_VALUE;i++)
		{
			System.out.println("i="+i+"->"+Float.intBitsToFloat(i));
		}
	}

	public static void testPadLeft() {

		byte[] padded;
		int largoPadded;
		largoPadded = 5;
		String texto;
		texto = "KOLA";
		padded = Utiles.PadLeft(texto, largoPadded);
		for (int i=0;i<largoPadded;i++)
		{
			System.out.print( "("+ ((char) padded[i] ) +")");
		}
	}

	/**
	 * Decodifica un arreglo generado por alguno de los  metodos:
	 *  byte[] getByteRepresentation()
	 *  byte[] getByteRepresentation(int)
	 * @param steim1
	 * @return
	 */
	public static int decodeByteRepresentation(byte[] steim1)
	{
		short largo = (short) steim1.length;
		int respuesta;
	
		byte[] fourBytes;
		fourBytes = new byte[4];
		for (int i=0;i<4;i++)
		{
			if (steim1[0]<0)
				fourBytes[i]=-1;
			else
				fourBytes[i]=0;
		}
		for (int i=0;i<largo;i++)
		{
			fourBytes[3-i]=steim1[largo-1-i];
		}		
		//		for (int i=0;i<4;i++)
		//		{
		//			System.out.print(" @@@ "+fourBytes[i]);
		//		}
		//		System.out.println();
	
		int k;
		respuesta=0;
		for (k=0; k<4; k++) {
			respuesta += fourBytes[k] << (8*(4-k-1));
		}
		return byteToInt(fourBytes);
	}

	//
	/**
	 * Decodifica un arreglo de bytes que representa un entero en notacion complemento de dos.
	 * http://www.velocityreviews.com/forums/t126357-how-to-convert-byte-into-a-single-integer.html
	 * no funciona correctamente con enteros en el rango negativo de 4 bits a cero.
	 * @param steim1
	 * @return
	 */
	public static int byteToInt(byte[] b)
	{
		int val=0;
		for (int i=b.length-1, j = 0; i >= 0; i--,j++)
		{
			val += (b[i] & 0xff) << (8*j);
		}
		return val;
	}



}

/**
 * 
 */
package cl.orbit.appendixb;

import java.util.Vector;

/**
 * @author sysop
 *
 */
public class Steim1Block {

	/**
	 * Aqui esta finalmente toda la data ingresada
	 */
	private byte[] steim1;


	/**
	 * Maquina de estados Finitos
	 */
	private Steim1DFA st1dfa;

	/**
	 * Samples que esta almacenando este bloque
	 * Comienza por X sub 0
	 * No olvidar que X sub -1 == lastsample pero no se almacena aqui. 
	 */
	private Vector<Integer> samples;

	/**
	 * Samples que no alcanzaron a entrar en este bloque
	 */
	private Vector<Integer> excedentSamples;

	/**
	 * D sub 0 = X sub 0 - X sub -1 
	 */
	private Vector<Integer> differences;

	/**
	 * Parejas Nibble Word contenidas en este bloque
	 */
	private Vector<Steim1CW> vCompress;

	/**
	 * Maximo de parejas CW (Nibble Word) que puede almacenar 
	 * este bloque en funcion de totalFrames
	 */
	private int maxCWSize;



	/**
	 * Valor del sampleo previo al primer sample nuevo
	 * de esta cadena.
	 */
	private int preSample;

	/**
	 * primer valor ingresado a esta cadena
	 */
	private int firstSample;

	/**
	 * Ultimo valor ingresado a esta cadena
	 */
	private int lastSample;

	/**
	 * Al terminar de agregar samples, este valor indica el indice del último sample agregado
	 */
	private int  indiceUltimoSampleBloque;

	/**
	 * Cantidad de filas de 16 Words
	 * 1 Word = 4 Bytes
	 */
	private int totalFrames;

	/**
	 * Cantidad de 2 bit Nibbles
	 */
	private int totalNibbles;

	public static final int DEFAULTFRAMES = 7;

	/**
	 * 
	 * @param frames
	 * @param lastSample 
	 */
	public Steim1Block (int frames, int preSample)
	{
		this.totalFrames = frames;
		this.steim1 = new byte [this.totalFrames * 64];
		this.preSample = preSample;
		this.lastSample = preSample; 
		this.maxCWSize = 13 + ((totalFrames-1) * 15);
		this.st1dfa = new Steim1DFA(0);
		samples = new Vector<Integer>();
		differences = new Vector<Integer>();
		vCompress = new Vector<Steim1CW>();
		this.excedentSamples = null;
	}

	/**
	 * agrega un nuevo sample.
	 * si no queda espacio para almcenar mas muestras, refresca un
	 * arreglo con los samples excedentes.
	 * @param newSample
	 * @return true si aun queda espacio teorico para almacenar, false en otro caso
	 */
	public boolean addSample(int nuevoSample) {
		boolean respuesta;
		if (samples.size()==0)
		{
			firstSample = nuevoSample;
		}
		this.samples.add(nuevoSample);
		respuesta = this.addDifference(nuevoSample -lastSample);
		lastSample = nuevoSample;
		// llena el arreglo con los samples que quedaron fuera de este bloque
		this.excedentSamples = null;
		if (respuesta==false)
		{
			Steim1CW ultimoCW;
			ultimoCW = this.vCompress.elementAt(maxCWSize-1);
			indiceUltimoSampleBloque = ultimoCW.getSampleIndex() + ultimoCW.getvEcoInt().size() -1;
			this.lastSample = samples.elementAt(indiceUltimoSampleBloque);
			//System.out.println("indiceUltimoSampleBloque="+indiceUltimoSampleBloque);
			this.excedentSamples = new Vector<Integer>();
			for (int i=(indiceUltimoSampleBloque+1);i<this.samples.size();i++)
			{
				this.excedentSamples.add(samples.elementAt(i));
			}
		}
		return respuesta;
	}

	/**
	 * 
	 * @param nuevaDiff
	 * @return true si aun queda espacio para almacenar, false en otro caso
	 */
	private boolean addDifference (int nuevaDiff)
	{
		boolean respuesta;
		respuesta = true;
		Steim1CW CW;
		CW = this.st1dfa.addDifference(nuevaDiff);
		this.differences.add(nuevaDiff);
		if (CW!=null) // Si se ha llenado una pareja CW
		{
			this.vCompress.add(CW); // Se añade la pareja al bloque
			if (vCompress.size()>=maxCWSize) {
				respuesta = false;
			}
			// Aqui se revisa si en la maquina de estado quedan valores
			Vector<EcoInt> vd; // Aqui se almacenan los valores excedentes
			vd = null;
			if (this.st1dfa.getDifferences().size()>0)
			{   // Solo se almacenan si hay mas de uno
				vd = this.st1dfa.getDifferences();
			}
			// Se reinicia la maquina de estados finitos de esta clase
			this.st1dfa = new Steim1DFA(this.samples.size());
			if (vd!=null) {
				for (int i=0;i<vd.size();i++) {
					// Para evitar que los mismos valores se repitan de manera incoherente
					this.differences.removeElementAt(this.differences.size()-1);
					this.st1dfa.decreaseSampleIndex();
					// Recursivamente se agregan los valores excedentes
					this.addDifference(vd.elementAt(i).getEntero());
				}
			}
		}
		return respuesta;
	}


	/**
	 * Vierte las estructuras almacenadas en esta clase en el arreglo Steim1
	 */
	public void refreshSteim1()
	{
		Steim1CW CW;// Pareja Codigo-Valore(s) con compresion Steim1
		short tipoCW;// Codigo de compresion (0,1,2 o3)
		int word0;// Primeros 4 bytes de cada frame (o fila) del bloque
		byte[] word_fourBytes;
		word_fourBytes = new byte[4];

		for (int frame=0;frame<totalFrames;frame++)	{
			word0 = 0;
			if (frame==0){
				for (int i=0;i<13;i++){
					CW = this.vCompress.elementAt(i);
					tipoCW = CW.getTipo();
					//System.out.println("tipoCW="+tipoCW);
					word0 += ( tipoCW << ((12-i)*2));
					word_fourBytes = CW.getWord();
					for (int j=0;j<4;j++) {
						steim1[12+(i*4)+j] = word_fourBytes[j];
					}
				}
			} else {
				int iStart, iEnd;
				iStart = 13+ (15*(frame-1));
				iEnd = iStart + 15;
				for (int i=iStart;i<iEnd;i++){
					CW = this.vCompress.elementAt(i);
					tipoCW = CW.getTipo();
					//System.out.println("tipoCW="+tipoCW);
					word0 += (tipoCW << ((30-(i-iStart))*2));
					//System.out.println("Word15="+word0+" -->"+Integer.toBinaryString(word0));
					word_fourBytes = CW.getWord();
					for (int j=0;j<4;j++) {
						steim1[(frame*64)+((i+1-iStart)*4)+j] = word_fourBytes[j];
					}
				}
			}
			//System.out.println("Word="+word0+" -->"+Integer.toBinaryString(word0));
			word_fourBytes = Utiles.IntToByteArray(word0, (short) 4);
			for (int i=0;i<4;i++){			
				steim1[(frame*64)+i]= word_fourBytes[i];
			}
		}

		// Escribimos el primer sample en notación 4 bytes
		word_fourBytes = Utiles.IntToByteArray(firstSample, (short) 4);
		for (int i=0;i<4;i++) {	
			steim1[4+i]= word_fourBytes[i];
		}
		// Escribimos el ultimo sample en notación 4 bytes
		word_fourBytes = Utiles.IntToByteArray(lastSample, (short) 4);
		for (int i=0;i<4;i++) {			
			steim1[8+i]= word_fourBytes[i];
		}
	}

	/**
	 * @return the steim1
	 */
	public byte[] getSteim1() {
		return steim1;
	}

	/**
	 * @param steim1 the steim1 to set
	 */
	public void setSteim1(byte[] steim1) {
		this.steim1 = steim1;
	}

	/**
	 * @return the firstSample
	 */
	public int getFirstSample() {
		return firstSample;
	}	

	/**
	 * @return the excedentSamples
	 */
	public Vector<Integer> getExcedentSamples() {
		return excedentSamples;
	}

	/**
	 * @param excedentSamples the excedentSamples to set
	 */
	public void setExcedentSamples(Vector<Integer> excedentSamples) {
		this.excedentSamples = excedentSamples;
	}

	/**
	 * @param firstSample the firstSample to set
	 */
	public void setFirstSample(int firstSample) {
		this.firstSample = firstSample;
	}

	/**
	 * @return the lastSample
	 */
	public int getLastSample() {
		return lastSample;
	}

	/**
	 * @param lastSample the lastSample to set
	 */
	public void setLastSample(int lastSample) {
		this.lastSample = lastSample;
	}

	/**
	 * @return the indiceUltimoSampleBloque
	 */
	public int getIndiceUltimoSampleBloque() {
		return indiceUltimoSampleBloque;
	}

	/**
	 * @param indiceUltimoSampleBloque the indiceUltimoSampleBloque to set
	 */
	public void setIndiceUltimoSampleBloque(int indiceUltimoSampleBloque) {
		this.indiceUltimoSampleBloque = indiceUltimoSampleBloque;
	}

	private static void test2() {
		Steim1Block st1;

		int cantErroresIndices;
		int cantErroresSamples;
		cantErroresIndices = 0; 
		cantErroresSamples = 0;
		int loop;
		loop = 0;
		RandomJump rj;
		rj = new RandomJump();

		for (;;)
		{
			st1= new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
			//System.out.println("maxCWSize="+st1.maxCWSize);
			boolean spaceAvailable;
			//			System.out.println("Agregando samples");
			do
			{
				int nuevoSample;
				nuevoSample = rj.next();
				spaceAvailable = st1.addSample(nuevoSample);
			} while (spaceAvailable);
			//			System.out.println("Desplegando Valores");
			//			System.out.println("presample="+st1.preSample);
			//			System.out.println("firstSample="+st1.firstSample);
			//			System.out.println("lastSample="+st1.lastSample);
			//			System.out.println("indiceUltimoSampleBloque="+st1.indiceUltimoSampleBloque);
			//
			//			System.out.println("--------------Samples en forma directa----------");
			//			for (int i=0;i<10;i++)
			//			{
			//				System.out.println("samples["+i+"]="+st1.samples.elementAt(i));
			//			}
			//			System.out.println("--------------Samples por descompresion Hacia Adelante----------");
			int valorLocal, valorSample;
			int index, posVei;
			valorLocal = st1.preSample;
			Steim1CW scw;
			Vector<EcoInt> vei;
			index = 0;
			boolean okIndice;
			boolean okSample;
			for (int i=0;i<st1.maxCWSize;i++)
			{
				//				System.out.println("____CW="+i+"_________");
				scw = st1.vCompress.elementAt(i);
				vei = scw.getvEcoInt();
				posVei = scw.getSampleIndex();
				for (int j=0;j<vei.size();j++)
				{
					valorSample = st1.samples.elementAt(index);
					valorLocal += vei.elementAt(j).getEntero();
					okIndice = (index==posVei);
					okSample = (valorSample==valorLocal);
					if (!okIndice) cantErroresIndices++;
					if (!okSample) cantErroresSamples++;
					//					System.out.print(" ;; Index("+index+"=="+posVei+")-->"+okIndice+"  sample("+valorSample+"=="+valorLocal+") OK="+okSample);
					index++;
					posVei++;
				}
				//				System.out.println("\n");
			}
			//			System.out.println("cantErroresIndices="+cantErroresIndices);
			//			System.out.println("cantErroresSamples="+cantErroresSamples);


			//			System.out.println("--------------Samples por descompresion Hacia Atras ----------");

			valorLocal = st1.lastSample;

			index = st1.indiceUltimoSampleBloque;

			for (int i=st1.maxCWSize-1;i>-1;i--)
			{
				//				System.out.println("____CW="+i+"_________");
				scw = st1.vCompress.elementAt(i);
				vei = scw.getvEcoInt();
				posVei = scw.getSampleIndex()+vei.size()-1;
				for (int j=vei.size()-1;j>-1;j--)
				{
					valorSample = st1.samples.elementAt(index);

					okIndice = (index==posVei);
					okSample = (valorSample==valorLocal);
					if (!okIndice) cantErroresIndices++;
					if (!okSample) cantErroresSamples++;
					//					System.out.print(" ;; Index("+index+"=="+posVei+")-->"+okIndice+"  sample("+valorSample+"=="+valorLocal+") OK="+okSample);
					index--;
					posVei--;
					valorLocal -= vei.elementAt(j).getEntero();
				}
				//				System.out.println("\n");
			}
			loop++;
			if ((loop%10000)==0)
			{
				System.out.print("Loop ("+loop+")");
				System.out.print("	cantErroresIndices="+cantErroresIndices);
				System.out.print("	cantErroresSamples="+cantErroresSamples);
				System.out.println("	indiceUltimoSampleBloque="+st1.indiceUltimoSampleBloque);
			}
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		test1();
	}

	private static void test1() {
		Steim1Block st1;
		st1= new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
		System.out.println("maxCWSize="+st1.maxCWSize);
		System.out.println("Agregando samples");
		boolean spaceAvailable;
		do
		{
			int nuevoSample;
			nuevoSample = (int) ((Math.random()-0.5)*25768);
			spaceAvailable = st1.addSample(nuevoSample);
		} while (spaceAvailable);
		System.out.println("Desplegando Valores");
		System.out.println("presample="+st1.preSample);
		System.out.println("firstSample="+st1.firstSample);
		System.out.println("lastSample="+st1.lastSample);
		System.out.println("indiceUltimoSampleBloque="+st1.indiceUltimoSampleBloque);

		System.out.println("--------------Samples en forma directa----------");
		for (int i=0;i<10;i++)
		{
			System.out.println("samples["+i+"]="+st1.samples.elementAt(i));
		}
		System.out.println("--------------Samples por descompresion Hacia Adelante----------");
		int valorLocal, valorSample;
		int index, posVei;
		valorLocal = st1.preSample;
		Steim1CW scw;
		Vector<EcoInt> vei;
		index = 0;
		boolean okIndice;
		boolean okSample;
		int cantErroresIndices;
		int cantErroresSamples;
		cantErroresIndices = 0; 
		cantErroresSamples = 0;
		for (int i=0;i<st1.maxCWSize;i++)
		{
			System.out.println("____CW="+i+"_________");
			scw = st1.vCompress.elementAt(i);
			vei = scw.getvEcoInt();
			posVei = scw.getSampleIndex();
			for (int j=0;j<vei.size();j++)
			{
				valorSample = st1.samples.elementAt(index);
				valorLocal += vei.elementAt(j).getEntero();
				okIndice = (index==posVei);
				okSample = (valorSample==valorLocal);
				if (!okIndice) cantErroresIndices++;
				if (!okSample) cantErroresSamples++;
				System.out.print(" ;; Index("+index+"=="+posVei+")-->"+okIndice+"  sample("+valorSample+"=="+valorLocal+") OK="+okSample);
				index++;
				posVei++;
			}
			System.out.println("\n");
		}
		System.out.println("cantErroresIndices="+cantErroresIndices);
		System.out.println("cantErroresSamples="+cantErroresSamples);


		System.out.println("--------------Samples por descompresion Hacia Atras ----------");

		valorLocal = st1.lastSample;

		index = st1.indiceUltimoSampleBloque;

		cantErroresIndices = 0; 
		cantErroresSamples = 0;
		for (int i=st1.maxCWSize-1;i>-1;i--)
		{
			System.out.println("____CW="+i+"_________");
			scw = st1.vCompress.elementAt(i);
			vei = scw.getvEcoInt();
			posVei = scw.getSampleIndex()+vei.size()-1;
			for (int j=vei.size()-1;j>-1;j--)
			{
				valorSample = st1.samples.elementAt(index);

				okIndice = (index==posVei);
				okSample = (valorSample==valorLocal);
				if (!okIndice) cantErroresIndices++;
				if (!okSample) cantErroresSamples++;
				System.out.print(" ;; Index("+index+"=="+posVei+")-->"+okIndice+"  sample("+valorSample+"=="+valorLocal+") OK="+okSample);
				index--;
				posVei--;
				valorLocal -= vei.elementAt(j).getEntero();
			}
			System.out.println("\n");
		}
		System.out.println("cantErroresIndices="+cantErroresIndices);
		System.out.println("cantErroresSamples="+cantErroresSamples);


	}

}

/**
 * 
 */
package cl.orbit.appendixb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Vector;

/**
 * @author sysop
 *
 */
public class SyntheticMiniSeed {

	private int currentSample;
	private Calendar currentTime;
	private Vector<Integer> sample;

	private Vector<Calendar> timestamp;

	private SeedHeader encabezadoSeed;
	private Blockette bloque1000;
	private Blockette bloque1001;
	private Steim1Block st1Block;
	private SequenceNumber seqNum;

	/**
	 * Periodo de tiempo entre muestras [seconds].
	 */
	private double sampleInterval;

	/**
	 * TimeStamp del primerSample
	 */
	private Calendar startTime;	

	public SyntheticMiniSeed(int samplesPerSecond)
	{
		System.out.println("Creando SyntheticMiniSeed.");
		
		sample = new Vector<Integer>();
		timestamp = new Vector<Calendar>();
		startTime = Calendar.getInstance();
		this.sampleInterval = (1.0/samplesPerSecond);
		System.out.println("sampleInterval="+sampleInterval);
		this.currentSample = 0;
		this.currentTime = (Calendar) startTime.clone();
		this.encabezadoSeed = new SeedHeader();
		seqNum = new SequenceNumber();
		seqNum.setIndice(123456);

		this.encabezadoSeed.setSeqNumber(seqNum);
		this.encabezadoSeed.setDataHeaderQualityIndicator((byte) 'D');
		this.encabezadoSeed.setStationCode(Utiles.PadLeft("SYNTH", 5));
		this.encabezadoSeed.setLocationIdentifier(Utiles.PadLeft("  ", 2));
		this.encabezadoSeed.setChannelIdentifier(Utiles.PadLeft("BHZ", 3));
		this.encabezadoSeed.setNetworkCode(Utiles.PadLeft("MS", 2));
		//this.encabezadoSeed.setRecordStartTime(new Btime());
		this.encabezadoSeed.setNumberOfSamples(Utiles.IntToByteArray(428,(short) 2));/////
		this.encabezadoSeed.setSampleRateFactor(Utiles.IntToByteArray(samplesPerSecond, (short) 2));/////sampleRate=20
		this.encabezadoSeed.setSampleRateMultiplier(Utiles.IntToByteArray(1, (short) 2));

		this.encabezadoSeed.setActivitiFlags((byte) 0);
		this.encabezadoSeed.setIoFlags((byte) 32);
		this.encabezadoSeed.setDataQualityFlags((byte) 0);
		this.encabezadoSeed.setNumberOfBlockettesThatFollow((byte)2);
		this.encabezadoSeed.setTimeCorrection(Utiles.IntToByteArray(0, (short)4));
		this.encabezadoSeed.setOffSetToBeginningOfData(Utiles.IntToByteArray(64, (short) 2));
		this.encabezadoSeed.setOffSetToBeginningOfFirstBlockette(Utiles.IntToByteArray(48, (short) 2));

		this.bloque1000 = Blockette1000.Steim1_512Bytes(56);
		this.bloque1001 = new Blockette1001(Utiles.IntToByteArray(0, (byte)2), (byte)100, (byte)7);

		this.st1Block = new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
	}

	/**
	 * Emulando una sinusoide en funcion del tiempo
	 */
	public void next()
	{
		long epoch;
		int nuevoSample;
		currentTime.add(Calendar.MILLISECOND,(int)( this.sampleInterval * 1000));
		epoch = currentTime.getTimeInMillis();
		nuevoSample = (int) (2500 * (Math.sin(epoch)));
		sample.add(nuevoSample);
		timestamp.add(currentTime);
	}



	/**
	 * @return the encabezadoSeed
	 */
	public SeedHeader getEncabezadoSeed() {
		return encabezadoSeed;
	}




	/**
	 * @param encabezadoSeed the encabezadoSeed to set
	 */
	public void setEncabezadoSeed(SeedHeader encabezadoSeed) {
		this.encabezadoSeed = encabezadoSeed;
	}




	/**
	 * @return the bloque1000
	 */
	public Blockette getBloque1000() {
		return bloque1000;
	}




	/**
	 * @param bloque1000 the bloque1000 to set
	 */
	public void setBloque1000(Blockette bloque1000) {
		this.bloque1000 = bloque1000;
	}




	/**
	 * @return the bloque1001
	 */
	public Blockette getBloque1001() {
		return bloque1001;
	}




	/**
	 * @param bloque1001 the bloque1001 to set
	 */
	public void setBloque1001(Blockette bloque1001) {
		this.bloque1001 = bloque1001;
	}




	/**
	 * @return the st1Block
	 */
	public Steim1Block getSt1Block() {
		return st1Block;
	}




	/**
	 * @param st1Block the st1Block to set
	 */
	public void setSt1Block(Steim1Block st1Block) {
		this.st1Block = st1Block;
	}

	private static void test1() {
		String archivoMiniSeed;
		archivoMiniSeed = "Synth.Miniseed";

		System.out.println("Creando Archivo:"+archivoMiniSeed);
		FileOutputStream fos;
		fos = null;
		try {
			fos = new FileOutputStream(archivoMiniSeed);
		} catch (FileNotFoundException e) {
			System.err.println("Error creando Archivo.");
			e.printStackTrace();
		}
		////////////////////////////////
		SyntheticMiniSeed sm;
		Vector<Integer> samplesExcedentes;
		//		Calendar fechaNextHeader;
		Calendar fechaHeader;
		sm =new SyntheticMiniSeed(20);

		double fractionSecondsInBlock;
		int adjustFractionSeconds;
		//int milliSecondsInBlock;
		int sample, i;
		sample =0;
		i = 0;
		samplesExcedentes = null;
		for (int loop=0;loop<80;loop++)
		{
			fechaHeader = sm.getEncabezadoSeed().getRecordStartTime().getFecha();
			System.out.println("Llenando Bloque Steim1.");
			if (samplesExcedentes!=null) {
				System.out.println(" ingresando samples exedentes de la iteracion anterior.");
				for (int k=0;k<samplesExcedentes.size();k++) { sm.getSt1Block().addSample(samplesExcedentes.elementAt(k)); }
			}
			while (sm.getSt1Block().addSample(sample)) { sample = (int) (100000 * Math.pow(-1, i) * Math.sin(i / 200.0) * Math.cos((i+140) / 200.0)); i++;}

			samplesExcedentes = sm.getSt1Block().getExcedentSamples();
			System.out.println("cant samplesExcedentes="+samplesExcedentes.size());
			System.out.println("Bloque Steim1 llenado.");
			int indiceUltimoSample;
			indiceUltimoSample = sm.getSt1Block().getIndiceUltimoSampleBloque();
			System.out.println("indiceUltimoSample="+indiceUltimoSample);
			sm.getEncabezadoSeed().setNumberOfSamples(Utiles.IntToByteArray(indiceUltimoSample+1, (short) 2));


			sm.guardar(fos);
			// Avandamos en 1 el contador de encabezados miniSeed
			sm.getEncabezadoSeed().getSeqNumber().next();
			// Actualizamos la fecha del proximo encabezado
			fractionSecondsInBlock = (sm.sampleInterval *10000.0* (indiceUltimoSample+1));
			//milliSecondsInBlock = (int) (fractionSecondsInBlock / 10.0);
			System.out.println("fractionSecondsInBlock="+fractionSecondsInBlock);
			fechaHeader.add(Calendar.MILLISECOND, (int) (fractionSecondsInBlock/10.0) );
			//fechaHeader.set(Calendar.MILLISECOND, 0);
			//sm.getEncabezadoSeed().getRecordStartTime().setFecha(fechaHeader);
//			adjustFractionSeconds = ( ( (int) fractionSecondsInBlock + 
//					sm.getEncabezadoSeed().getRecordStartTime().getSecondFraction()
//			)
//			%10000);
//			sm.getEncabezadoSeed().getRecordStartTime().setSecondFraction( adjustFractionSeconds );
			// Blanqueamos el bloque Steim1
			sm.st1Block = new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
		}
		////////////////////////////////
		System.out.println("Cerrando Archivo.");
		try {
			fos.close();
		} catch (IOException e) {
			System.err.println("Error Cerrando Archivo.");
			e.printStackTrace();
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		test1();
		//		SeedHeader sh;
		//		SequenceNumber seqNum;
		//		seqNum = new SequenceNumber();
		//		seqNum.setIndice(501675);
		//		String archivoMiniSeed;
		//		archivoMiniSeed = "PB73Miniseed";
		//		sh = new SeedHeader();
		//		sh.setSeqNumber(seqNum);
		//		sh.setDataHeaderQualityIndicator((byte) 'D');
		//		sh.setStationCode(Utiles.PadLeft("PB73", 5));
		//		sh.setLocationIdentifier(Utiles.PadLeft("  ", 2));
		//		sh.setChannelIdentifier(Utiles.PadLeft("BHZ", 3));
		//		sh.setNetworkCode(Utiles.PadLeft("CX", 2));
		//		sh.setRecordStartTime(new Btime());
		//		sh.setNumberOfSamples(Utiles.IntToByteArray(428,(short) 2));
		//		sh.setSampleRateFactor(Utiles.IntToByteArray(20, (short) 2));
		//		sh.setSampleRateMultiplier(Utiles.IntToByteArray(1, (short) 2));
		//
		//		sh.setActivitiFlags((byte) 0);
		//		sh.setIoFlags((byte) 32);
		//		sh.setDataQualityFlags((byte) 0);
		//		sh.setNumberOfBlockettesThatFollow((byte)2);
		//		sh.setTimeCorrection(Utiles.IntToByteArray(0, (short)4));
		//		sh.setOffSetToBeginningOfData(Utiles.IntToByteArray(64, (short) 2));
		//		sh.setOffSetToBeginningOfFirstBlockette(Utiles.IntToByteArray(48, (short) 2));

		///////////////////////////////////////
		/*
		System.out.println("Escribiendo SeedHeader en archivo:"+archivoMiniSeed);
		//System.out.println(Utiles.toString(sh.toByteArray()));
		FileOutputStream fos;
		fos = null;
		try {
			fos = new FileOutputStream(archivoMiniSeed);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fos.write(sh.toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Blockette bl1000;
		bl1000 = Blockette1000.Steim1_512Bytes(56);
		try {
			fos.write(((Blockette1000)bl1000).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Blockette bl1001;
		bl1001 = new Blockette1001(Utiles.IntToByteArray(0, (byte)2), (byte)100, (byte)7);
		try {
			fos.write(  ((Blockette1001)bl1001).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
		///////////////////////////////////////

	}

	/**
	 * Guarda 4 cadenas de bytes de manera consecutiva.
	 * i) Encabezado Seed
	 * ii) Blockette 1000
	 * iii) Blockette 1001
	 * iv) Bloque Steim1
	 * En general el total de estas cadenas debe sumar 512 bytes
	 * @param fos Stream donde se deben escribir los datos
	 */
	public void guardar(FileOutputStream fos)
	{

		System.out.println("Escribiendo encabezadoSeed.");
		try {
			fos.write(this.encabezadoSeed.toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo encabezadoSeed.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Blockete1000");
		try {
			fos.write( ((Blockette1000)this.bloque1000).toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo Blockete1000.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Blockette1001.");
		try {
			fos.write(((Blockette1001)this.bloque1001).toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo Blockette1001.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Bloque Steim1.");
		try {
			this.st1Block.refreshSteim1();
			fos.write(this.st1Block.getSteim1());
		} catch (IOException e) {
			System.err.println("Error escribiendo Bloque Steim1.");
			e.printStackTrace();
		}
	}

}

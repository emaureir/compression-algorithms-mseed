/**
 * 
 */
package cl.orbit.appendixb;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.Vector;

/**
 * @author sysop
 *
 */
public class TcpMiniSeed {

	private Calendar currentTime;
	private Vector<Integer> sample;

	private Vector<Calendar> timestamp;

	private SeedHeader[] encabezadoSeed;//ZEN
	private Blockette bloque1000;
	private Blockette bloque1001;
	private Steim1Block[] st1Block;//ZEN
	private SequenceNumber[] seqNum;//ZEN
	private StaLta groundEnergy;

	/**
	 * Periodo de tiempo entre muestras [seconds].
	 */
	private double sampleInterval;

	/**
	 * TimeStamp del primerSample
	 */
	private Calendar startTime;	

	private ServerSocket serverSsocket;

	private Socket socket;

	private BufferedReader reader;

	private String stationCODE;
	private String[] channels;

	public TcpMiniSeed(int samplesPerSecond, String stationCODE, String[] channels)
	{
		this.stationCODE = stationCODE;
		this.channels = channels;
		int cantChannels;
		cantChannels = this.channels.length;
		
		this.groundEnergy = new StaLta( 0.5, 2.0);
		
		encabezadoSeed = new SeedHeader[cantChannels];
		st1Block = new Steim1Block[cantChannels];
		seqNum = new SequenceNumber[cantChannels];
		fechaHeader = new Calendar[cantChannels];
		for (int i=0;i<cantChannels;i++)
		{
			this.seqNum[i] = new SequenceNumber();
			this.seqNum[i].setIndice(123456);
			this.st1Block[i] = new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
			this.encabezadoSeed[i]= TcpMiniSeed.creaEncabezado("MS", this.stationCODE, "  ", this.channels[i], samplesPerSecond, this.seqNum[i]);
			this.fechaHeader[i] = this.encabezadoSeed[i].getRecordStartTime().getFecha();
		}

		try {
			serverSsocket = new ServerSocket(36524);
		} catch (IOException e) {
			System.err.println("Error Creando Socket");
			e.printStackTrace();
		}
		preparaTcp();

		System.out.println("Creando SyntheticMiniSeed.");

		sample = new Vector<Integer>();
		timestamp = new Vector<Calendar>();
		startTime = Calendar.getInstance();
		this.sampleInterval = (1.0/samplesPerSecond);
		System.out.println("sampleInterval="+sampleInterval);

		this.currentTime = (Calendar) startTime.clone();

		this.bloque1000 = Blockette1000.Steim1_512Bytes(56);
		this.bloque1001 = new Blockette1001(Utiles.IntToByteArray(0, (byte)2), (byte)100, (byte)7);

	}

	public static SeedHeader creaEncabezado (String networkCODE, String statiCODE, String locCode, String channelCODE, int samplesPerSecond, SequenceNumber sequencNumber)
	{
		SeedHeader respuesta;
		respuesta = new SeedHeader();
		respuesta.setSeqNumber(sequencNumber);
		respuesta.setDataHeaderQualityIndicator((byte) 'D');
		respuesta.setStationCode(Utiles.PadLeft(statiCODE, 5));
		respuesta.setLocationIdentifier(Utiles.PadLeft(locCode, 2));
		respuesta.setChannelIdentifier(Utiles.PadLeft(channelCODE, 3));
		respuesta.setNetworkCode(Utiles.PadLeft(networkCODE, 2));

		respuesta.setNumberOfSamples(Utiles.IntToByteArray(428,(short) 2));/////
		respuesta.setSampleRateFactor(Utiles.IntToByteArray(samplesPerSecond, (short) 2));/////sampleRate=20
		respuesta.setSampleRateMultiplier(Utiles.IntToByteArray(1, (short) 2));

		respuesta.setActivitiFlags((byte) 0);
		respuesta.setIoFlags((byte) 32);
		respuesta.setDataQualityFlags((byte) 0);
		respuesta.setNumberOfBlockettesThatFollow((byte)2);
		respuesta.setTimeCorrection(Utiles.IntToByteArray(0, (short)4));
		respuesta.setOffSetToBeginningOfData(Utiles.IntToByteArray(64, (short) 2));
		respuesta.setOffSetToBeginningOfFirstBlockette(Utiles.IntToByteArray(48, (short) 2));
		return respuesta;
	}

	public void preparaTcp()
	{
		System.out.println("Esperando Cliente TCP");
		try {
			socket = serverSsocket.accept();
		} catch (IOException e) {
			System.err.println("Error Esperando Cliente TCP");
			e.printStackTrace();
		}
		System.out.println("Creando Lector TCP.");
		try {
			reader = new BufferedReader (new InputStreamReader (socket.getInputStream()));
		} catch (IOException e) {
			System.err.println("Error creando Lector TCP.");
			e.printStackTrace();
		}
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Emulando una sinusoide en funcion del tiempo
	 */
	public void next()
	{
		long epoch;
		int nuevoSample;
		currentTime.add(Calendar.MILLISECOND,(int)( this.sampleInterval * 1000));
		epoch = currentTime.getTimeInMillis();
		nuevoSample = (int) (2500 * (Math.sin(epoch)));
		sample.add(nuevoSample);
		timestamp.add(currentTime);
	}

	/**
	 * @return the bloque1000
	 */
	public Blockette getBloque1000() {
		return bloque1000;
	}

	/**
	 * @param bloque1000 the bloque1000 to set
	 */
	public void setBloque1000(Blockette bloque1000) {
		this.bloque1000 = bloque1000;
	}


	/**
	 * @return the bloque1001
	 */
	public Blockette getBloque1001() {
		return bloque1001;
	}




	/**
	 * @param bloque1001 the bloque1001 to set
	 */
	public void setBloque1001(Blockette bloque1001) {
		this.bloque1001 = bloque1001;
	}

	private Calendar[] fechaHeader;

	private void test1() {

		float[] sample;
		long timeStamp;
		int valorInt;
		sample = new float[3];
		sample[0] = sample[1] = sample[2] = 0;
		System.out.println ("Llenando Bloque Steim1.");
		while (true) {
			//				sample = (int) (100000 * Math.pow(-1, i) * Math.sin(i / 200.0) * Math.cos((i+140) / 200.0)); i++;
			String linea;
			String[] chop;
			linea  = null;
			try {
				linea = reader.readLine();
				if (linea==null)
				{
					System.out.println("Comunicaciones Interrumpidas");
					break;
				}
				if (linea!="---tick")
				{
//					System.out.println(linea);
					chop = linea.split("\t");
					if (chop.length==4)
					{
						timeStamp = Long.parseLong(chop[0]);
						for (int i=0;i<3;i++)
						{
							sample[i] = Float.parseFloat(chop[1+i]);
							///valorInt = Float.floatToIntBits(sample[i]);
							valorInt = (int) ((sample[i]) * 100000);
							if (!(st1Block[i].addSample( valorInt )  ) )
							{
								this.ProcesaSteimLleno(i);
							}
						}
						SensorEvent sevent;
						sevent = new SensorEvent(timeStamp,sample);
						this.groundEnergy.addSample(sevent);
						System.out.print(this.groundEnergy.toString());
					}
				}
				//System.out.println(linea);
			} catch (IOException e) {

				e.printStackTrace();
				break;
			}
		}
	}

	private void ProcesaSteimLleno(int canal)
	{
		double fractionSecondsInBlock;

		StringBuilder archivoMiniSeed;
		Vector<Integer> samplesExcedentes;
		samplesExcedentes = st1Block[canal].getExcedentSamples();
		System.out.println("cant samplesExcedentes="+samplesExcedentes.size());
		System.out.println("Bloque Steim1 llenado.");
		int indiceUltimoSample;
		indiceUltimoSample = st1Block[canal].getIndiceUltimoSampleBloque();
		System.out.println("indiceUltimoSample="+indiceUltimoSample);
		encabezadoSeed[canal].setNumberOfSamples(Utiles.IntToByteArray(indiceUltimoSample+1, (short) 2));

		archivoMiniSeed = new StringBuilder();
		archivoMiniSeed.append("/home/sysop/Documents/tmp/mseed/");
		archivoMiniSeed.append(encabezadoSeed[canal].getSeqNumber().getIndice()%1000);
		archivoMiniSeed.append("Synth.");
		archivoMiniSeed.append(this.channels[canal]);

		System.out.println("Creando Archivo:"+archivoMiniSeed);
		FileOutputStream fos;
		fos = null;
		try {
			fos = new FileOutputStream(archivoMiniSeed.toString());
		} catch (FileNotFoundException e) {
			System.err.println("Error creando Archivo.");
			e.printStackTrace();
		}

		guardar(fos,canal);
		System.out.println("Cerrando Archivo.");
		try {
			fos.close();
		} catch (IOException e) {
			System.err.println("Error Cerrando Archivo.");
			e.printStackTrace();
		}

		// Avandamos en 1 el contador de encabezados miniSeed
		encabezadoSeed[canal].getSeqNumber().next();
		// Actualizamos la fecha del proximo encabezado
		fractionSecondsInBlock = (sampleInterval *10000.0* (indiceUltimoSample+1));
		//milliSecondsInBlock = (int) (fractionSecondsInBlock / 10.0);
		System.out.println("fractionSecondsInBlock="+fractionSecondsInBlock);
		fechaHeader[canal].add(Calendar.MILLISECOND, (int) (fractionSecondsInBlock/10.0) );
		// Blanqueamos el bloque Steim1
		st1Block[canal] = new Steim1Block(Steim1Block.DEFAULTFRAMES, -2);
		fechaHeader[canal] = encabezadoSeed[canal].getRecordStartTime().getFecha();
		if (samplesExcedentes!=null) {
			System.out.println(" ingresando ["+samplesExcedentes.size()+"] samples exedentes de la iteracion anterior.");
			for (int k=0;k<samplesExcedentes.size();k++) { st1Block[0].addSample(samplesExcedentes.elementAt(k)); }
		}
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		TcpMiniSeed sm;
		String[] channels;
		channels = new String[3];
		channels[0] ="BNZ";
		channels[1] ="BNE";
		channels[2] ="BNN";

		sm =new TcpMiniSeed(50,"AND01",channels);
		sm.test1();
		//		SeedHeader sh;
		//		SequenceNumber seqNum;
		//		seqNum = new SequenceNumber();
		//		seqNum.setIndice(501675);
		//		String archivoMiniSeed;
		//		archivoMiniSeed = "PB73Miniseed";
		//		sh = new SeedHeader();
		//		sh.setSeqNumber(seqNum);
		//		sh.setDataHeaderQualityIndicator((byte) 'D');
		//		sh.setStationCode(Utiles.PadLeft("PB73", 5));
		//		sh.setLocationIdentifier(Utiles.PadLeft("  ", 2));
		//		sh.setChannelIdentifier(Utiles.PadLeft("BHZ", 3));
		//		sh.setNetworkCode(Utiles.PadLeft("CX", 2));
		//		sh.setRecordStartTime(new Btime());
		//		sh.setNumberOfSamples(Utiles.IntToByteArray(428,(short) 2));
		//		sh.setSampleRateFactor(Utiles.IntToByteArray(20, (short) 2));
		//		sh.setSampleRateMultiplier(Utiles.IntToByteArray(1, (short) 2));
		//
		//		sh.setActivitiFlags((byte) 0);
		//		sh.setIoFlags((byte) 32);
		//		sh.setDataQualityFlags((byte) 0);
		//		sh.setNumberOfBlockettesThatFollow((byte)2);
		//		sh.setTimeCorrection(Utiles.IntToByteArray(0, (short)4));
		//		sh.setOffSetToBeginningOfData(Utiles.IntToByteArray(64, (short) 2));
		//		sh.setOffSetToBeginningOfFirstBlockette(Utiles.IntToByteArray(48, (short) 2));

		///////////////////////////////////////
		/*
		System.out.println("Escribiendo SeedHeader en archivo:"+archivoMiniSeed);
		//System.out.println(Utiles.toString(sh.toByteArray()));
		FileOutputStream fos;
		fos = null;
		try {
			fos = new FileOutputStream(archivoMiniSeed);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fos.write(sh.toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Blockette bl1000;
		bl1000 = Blockette1000.Steim1_512Bytes(56);
		try {
			fos.write(((Blockette1000)bl1000).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Blockette bl1001;
		bl1001 = new Blockette1001(Utiles.IntToByteArray(0, (byte)2), (byte)100, (byte)7);
		try {
			fos.write(  ((Blockette1001)bl1001).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 */
		///////////////////////////////////////

	}

	/**
	 * Guarda 4 cadenas de bytes de manera consecutiva.
	 * i) Encabezado Seed
	 * ii) Blockette 1000
	 * iii) Blockette 1001
	 * iv) Bloque Steim1
	 * En general el total de estas cadenas debe sumar 512 bytes
	 * @param fos Stream donde se deben escribir los datos
	 */
	public void guardar(FileOutputStream fos, int channelID)
	{

		System.out.println("Escribiendo encabezadoSeed.");
		try {
			fos.write(this.encabezadoSeed[channelID].toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo encabezadoSeed.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Blockete1000");
		try {
			fos.write( ((Blockette1000)this.bloque1000).toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo Blockete1000.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Blockette1001.");
		try {
			fos.write(((Blockette1001)this.bloque1001).toByteArray());
		} catch (IOException e) {
			System.err.println("Error escribiendo Blockette1001.");
			e.printStackTrace();
		}
		System.out.println("Escribiendo Bloque Steim1.");
		try {
			this.st1Block[channelID].refreshSteim1();
			fos.write(this.st1Block[channelID].getSteim1());
		} catch (IOException e) {
			System.err.println("Error escribiendo Bloque Steim1.");
			e.printStackTrace();
		}
	}

}

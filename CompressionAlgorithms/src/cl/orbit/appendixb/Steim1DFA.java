/**
 * Se toma como referencia el "Norwegian_Technical_18.pdf" disponible en:
 * http://www.sara.pg.it/oth_docs/Norwegian_Technical_18.pdf
 */
package cl.orbit.appendixb;

import java.io.IOException;
import java.util.Vector;

/**
 * @author sysop
 *
 */
public class Steim1DFA {

	private int estado;
	/**
	 * FIFO
	 */
	private Vector<EcoInt> differences;

	/**
	 * Cuando el metodo addDifference crea una instancia de Steim1CW,
	 * este indice sirve para deducir a que sample del bloque corresponden
	 * cada uno de los valores almacenados en el  Steim1CW.
	 */
	private int sampleIndex;

	public static final short STATE_0=0;
	public static final short STATE_10=1;
	public static final short STATE_4=2;
	public static final short STATE_20=3;
	public static final short STATE_110=4;
	public static final short STATE_22=5;
	public static final short STATE_999=6;
	public static final short STATE_1110=7;
	public static final short STATE_1111=8;

	public Steim1DFA(int sampleIndex) {
		this.estado = STATE_0;
		this.sampleIndex = sampleIndex;
		differences = new Vector<EcoInt>();
	}

	/**
	 * Añade una nueva diferencia al vector de diferencias.
	 * 
	 * Segun el tipo de compresion Aplicable a la nueva diferencia,
	 * desplaza la Maquina de estados finitos a su siguiente estado. 
	 * 
	 * @param diff Diferencia entre dos samples consecutivos.
	 * @return pareja CW con compresion Steim1 eficiente.
	 */
	public Steim1CW addDifference (int diff)
	{
		Steim1CW s1cwRespuesta;
		s1cwRespuesta = null;
		EcoInt ei = new EcoInt(diff);
		this.differences.add(ei);
		switch (estado) {
		case STATE_0:
			if (ei.getMinSteim1Bytes()==1) estado = STATE_10;
			if (ei.getMinSteim1Bytes()==2) estado = STATE_20;
			if (ei.getMinSteim1Bytes()==4) estado = STATE_4;
			break;
		case STATE_10:
			if (ei.getMinSteim1Bytes()==1) estado = STATE_110;
			if (ei.getMinSteim1Bytes()==2) estado = STATE_22;
			if (ei.getMinSteim1Bytes()==4) estado = STATE_4;
			break;
		case STATE_4:			
			s1cwRespuesta = new Steim1CW(Steim1CW.ONE_4Byte, this.sampleIndex);
			s1cwRespuesta.add(this.differences.remove(0));
			estado = STATE_999;
			break;
		case STATE_20:
			if (ei.getMinSteim1Bytes()==1) estado = STATE_22;
			if (ei.getMinSteim1Bytes()==2) estado = STATE_22;
			if (ei.getMinSteim1Bytes()==4) estado = STATE_4;
			break;
		case STATE_110:
			if (ei.getMinSteim1Bytes()==1) estado = STATE_1110;
			if (ei.getMinSteim1Bytes()==2) estado = STATE_4;
			if (ei.getMinSteim1Bytes()==4) estado = STATE_4;
			break;
		case STATE_22:
			s1cwRespuesta = new Steim1CW(Steim1CW.TWO_2Byte, this.sampleIndex);
			s1cwRespuesta.add(this.differences.remove(0));
			s1cwRespuesta.add(this.differences.remove(0));
			estado = STATE_999;
			break;
		case STATE_999:
			break;
		case STATE_1110:
			if (ei.getMinSteim1Bytes()==1) estado = STATE_1111;
			if (ei.getMinSteim1Bytes()==2) estado = STATE_22;
			if (ei.getMinSteim1Bytes()==4) estado = STATE_22;
			break;
		case STATE_1111:
			s1cwRespuesta = new Steim1CW(Steim1CW.FOUR_1Byte, this.sampleIndex);
			s1cwRespuesta.add(this.differences.remove(0));
			s1cwRespuesta.add(this.differences.remove(0));
			s1cwRespuesta.add(this.differences.remove(0));
			s1cwRespuesta.add(this.differences.remove(0));
			estado = STATE_999;
			break;
		}
		return s1cwRespuesta;
	}

	public void decreaseSampleIndex()
	{
		this.sampleIndex--;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Vector<EcoInt> getDifferences() {
		return differences;
	}

	public void setDifferences(Vector<EcoInt> differences) {
		this.differences = differences;
	}

	public int getSampleIndex() {
		return sampleIndex;
	}

	public void setSampleIndex(int sampleIndex) {
		this.sampleIndex = sampleIndex;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int test;
		test = 1;
		switch (test) {
		case 1:
			test1();
			break;
		case 2:
			test2();
			break;
		case 3:
			test3();
			break;

		default:
			test1();
			break;
		}

	}

	private static void test1() {
		System.out.println("Test 1.");
		Steim1DFA s1dfa;
		s1dfa = new Steim1DFA(0);
		java.io.BufferedReader stdin = new java.io.BufferedReader(new java.io.InputStreamReader(System.in));
		for(;;)
		{
			System.out.println("\n-----------------------\nOpciones:");
			System.out.println("1) agrega 1BYTE number.");
			System.out.println("2) agrega 2BYTE number.");
			System.out.println("4) agrega 4BYTE number.");
			System.out.println("0) Salir.");
			String userInput;
			userInput = "100";
			System.out.print("Estado Actual = "+s1dfa.estado);
			System.out.print("  //?");
			try {
				userInput = stdin.readLine();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			int userOption;
			userOption = 100;
			try {
				userOption = Integer.parseInt(userInput);
			} catch (NumberFormatException e) {
				// TODO: handle exception
			}
			Steim1CW decision;
			decision = null;
			switch (userOption) {
			case 1:
				decision = s1dfa.addDifference(36);
				break;
			case 2:
				decision = s1dfa.addDifference(360);
				break;
			case 4:
				decision = s1dfa.addDifference(32768);
				break;
			}			
			if (userOption==0) break;
			if (decision!=null) 
			{ 
				System.out.println("Se ha creado un Steim1CW tipo"+decision.getTipo());
			}
			System.out.println("differences"+s1dfa.differences);
		}

	}

	private static void test2() {
		// TODO Auto-generated method stub

	}

	private static void test3() {
		// TODO Auto-generated method stub

	}



}

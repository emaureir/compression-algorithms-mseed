/**
 * 
 */
package cl.orbit.appendixb;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author sysop
 *
 */
public class SeedHeader {

	private SequenceNumber seqNumber;
	private byte dataHeaderQualityIndicator;
	private byte reserved;
	private byte[] stationCode;
	private byte[] locationIdentifier;
	private byte[]	channelIdentifier;
	private byte[] networkCode;
	private Btime	recordStartTime;
	private byte[]	numberOfSamples;
	private byte[]	sampleRateFactor;
	private byte[]	sampleRateMultiplier;
	private byte	activitiFlags;
	private byte	ioFlags;
	private byte	dataQualityFlags;
	private byte	numberOfBlockettesThatFollow;
	private byte[]	timeCorrection;
	private byte[]	offSetToBeginningOfData;
	private byte[]	offSetToBeginningOfFirstBlockette;


	public byte[] toByteArray ()
	{
		byte[] respuesta = new byte[48];
		int i=0;
		byte[] seqNum, binaryTime;
		seqNum = this.seqNumber.toByteArray();
		for (int j=0;j<6;j++) {	respuesta[i] = seqNum[j];	i++; }

		respuesta[i] = dataHeaderQualityIndicator; i++;
		respuesta[i] = reserved; i++;
		for (int j=0;j<stationCode.length;j++) {	respuesta[i] = stationCode[j];	i++; }
		for (int j=0;j<locationIdentifier.length;j++) {	respuesta[i] = locationIdentifier[j];	i++; }
		for (int j=0;j<channelIdentifier.length;j++) {	respuesta[i] = channelIdentifier[j];	i++; }
		for (int j=0;j<networkCode.length;j++) {	respuesta[i] = networkCode[j];	i++; }

		binaryTime = this.recordStartTime.toByteArray();

		for (int j=0;j<binaryTime.length;j++) {	respuesta[i] = binaryTime[j];	i++; }
		for (int j=0;j<numberOfSamples.length;j++) {	respuesta[i] = numberOfSamples[j];	i++; }
		for (int j=0;j<sampleRateFactor.length;j++) {	respuesta[i] = sampleRateFactor[j];	i++; }
		for (int j=0;j<sampleRateMultiplier.length;j++) {	respuesta[i] = sampleRateMultiplier[j];	i++; }
		respuesta[i] = activitiFlags; i++;
		respuesta[i] = ioFlags; i++;
		respuesta[i] = dataQualityFlags; i++;
		respuesta[i] = numberOfBlockettesThatFollow; i++;
		for (int j=0;j<timeCorrection.length;j++) {	respuesta[i] = timeCorrection[j];	i++; }
		for (int j=0;j<offSetToBeginningOfData.length;j++) {	respuesta[i] = offSetToBeginningOfData[j];	i++; }
		for (int j=0;j<offSetToBeginningOfFirstBlockette.length;j++) {	respuesta[i] = offSetToBeginningOfFirstBlockette[j];	i++; }
		return respuesta;
	}

	public SeedHeader ()
	{
		this.seqNumber			= new SequenceNumber();
		this.dataHeaderQualityIndicator = (byte) ('D');
		this.reserved = 32;
		this.stationCode		= new byte[5];
		this.locationIdentifier	= new byte[2];
		this.channelIdentifier	= new byte[3];
		this.networkCode		= new byte[2];
		this.recordStartTime	= new Btime(0);
		this.numberOfSamples	= new byte[2];
		this.sampleRateFactor	= new byte[2];
		this.sampleRateMultiplier				= new byte[2];
		this.timeCorrection						= new byte[4];
		this.offSetToBeginningOfData			= new byte[2];
		this.offSetToBeginningOfFirstBlockette	= new byte[2];

	}	

	public byte getReserved() {
		return reserved;
	}

	public void setReserved(byte reserved) {
		this.reserved = reserved;
	}

	public byte[] getNumberOfSamples() {
		return numberOfSamples;
	}

	public void setNumberOfSamples(byte[] numberOfSamples) {
		this.numberOfSamples = numberOfSamples;
	}

	public byte[] getSampleRateFactor() {
		return sampleRateFactor;
	}

	public void setSampleRateFactor(byte[] sampleRateFactor) {
		this.sampleRateFactor = sampleRateFactor;
	}

	public byte[] getSampleRateMultiplier() {
		return sampleRateMultiplier;
	}

	public void setSampleRateMultiplier(byte[] sampleRateMultiplier) {
		this.sampleRateMultiplier = sampleRateMultiplier;
	}

	public byte getActivitiFlags() {
		return activitiFlags;
	}

	public void setActivitiFlags(byte activitiFlags) {
		this.activitiFlags = activitiFlags;
	}

	public byte getIoFlags() {
		return ioFlags;
	}

	public void setIoFlags(byte ioFlags) {
		this.ioFlags = ioFlags;
	}

	public byte getDataQualityFlags() {
		return dataQualityFlags;
	}

	public void setDataQualityFlags(byte dataQualityFlags) {
		this.dataQualityFlags = dataQualityFlags;
	}

	public byte getNumberOfBlockettesThatFollow() {
		return numberOfBlockettesThatFollow;
	}

	public void setNumberOfBlockettesThatFollow(byte numberOfBlockettesThatFollow) {
		this.numberOfBlockettesThatFollow = numberOfBlockettesThatFollow;
	}

	public byte[] getTimeCorrection() {
		return timeCorrection;
	}

	public void setTimeCorrection(byte[] timeCorrection) {
		this.timeCorrection = timeCorrection;
	}

	public byte[] getOffSetToBeginningOfData() {
		return offSetToBeginningOfData;
	}

	public void setOffSetToBeginningOfData(byte[] offSetToBeginningOfData) {
		this.offSetToBeginningOfData = offSetToBeginningOfData;
	}

	public byte[] getOffSetToBeginningOfFirstBlockette() {
		return offSetToBeginningOfFirstBlockette;
	}

	public void setOffSetToBeginningOfFirstBlockette(
			byte[] offSetToBeginningOfFirstBlockette) {
		this.offSetToBeginningOfFirstBlockette = offSetToBeginningOfFirstBlockette;
	}

	public Btime getRecordStartTime() {
		return recordStartTime;
	}

	public void setRecordStartTime(Btime recordStartTime) {
		this.recordStartTime = recordStartTime;
	}

	public SequenceNumber getSeqNumber() {
		return seqNumber;
	}

	public void setSeqNumber(SequenceNumber seqNumber) {
		this.seqNumber = seqNumber;
	}

	public byte getDataHeaderQualityIndicator() {
		return dataHeaderQualityIndicator;
	}

	public void setDataHeaderQualityIndicator(byte dataHeaderQualityIndicator) {
		this.dataHeaderQualityIndicator = dataHeaderQualityIndicator;
	}

	public byte[] getStationCode() {
		return stationCode;
	}

	public void setStationCode(byte[] stationCode) {
		this.stationCode = stationCode;
	}

	public byte[] getLocationIdentifier() {
		return locationIdentifier;
	}

	public void setLocationIdentifier(byte[] locationIdentifier) {
		this.locationIdentifier = locationIdentifier;
	}

	public byte[] getChannelIdentifier() {
		return channelIdentifier;
	}

	public void setChannelIdentifier(byte[] channelIdentifier) {
		this.channelIdentifier = channelIdentifier;
	}

	public byte[] getNetworkCode() {
		return networkCode;
	}

	public void setNetworkCode(byte[] networkCode) {
		this.networkCode = networkCode;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SeedHeader sh;
		SequenceNumber seqNum;
		seqNum = new SequenceNumber();
		seqNum.setIndice(501675);
		String archivoMiniSeed;
		archivoMiniSeed = "PB73Miniseed";
		sh = new SeedHeader();
		sh.setSeqNumber(seqNum);
		sh.setDataHeaderQualityIndicator((byte) 'D');
		sh.setStationCode(Utiles.PadLeft("PB73", 5));
		sh.setLocationIdentifier(Utiles.PadLeft("  ", 2));
		sh.setChannelIdentifier(Utiles.PadLeft("BHZ", 3));
		sh.setNetworkCode(Utiles.PadLeft("CX", 2));
		sh.setRecordStartTime(new Btime(0));
		sh.setNumberOfSamples(Utiles.IntToByteArray(428,(short) 2));
		sh.setSampleRateFactor(Utiles.IntToByteArray(20, (short) 2));
		sh.setSampleRateMultiplier(Utiles.IntToByteArray(1, (short) 2));

		sh.setActivitiFlags((byte) 0);
		sh.setIoFlags((byte) 32);
		sh.setDataQualityFlags((byte) 0);
		sh.setNumberOfBlockettesThatFollow((byte)2);
		sh.setTimeCorrection(Utiles.IntToByteArray(0, (short)4));
		sh.setOffSetToBeginningOfData(Utiles.IntToByteArray(64, (short) 2));
		sh.setOffSetToBeginningOfFirstBlockette(Utiles.IntToByteArray(48, (short) 2));
		System.out.println("Escribiendo SeedHeader en archivo:"+archivoMiniSeed);
		//System.out.println(Utiles.toString(sh.toByteArray()));
		FileOutputStream fos;
		fos = null;
		try {
			fos = new FileOutputStream(archivoMiniSeed);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			fos.write(sh.toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Blockette bl1000;
		bl1000 = Blockette1000.Steim1_512Bytes(56);
		try {
			fos.write(((Blockette1000)bl1000).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Blockette bl1001;
		bl1001 = new Blockette1001(Utiles.IntToByteArray(0, (byte)2), (byte)100, (byte)7);
		try {
			fos.write(  ((Blockette1001)bl1001).toByteArray());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

}

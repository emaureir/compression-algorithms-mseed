package cl.orbit.appendixb;

import java.util.Vector;

/**
 * Se toma como referencia el "SEEDManual_V2.4.pdf" disponible en:
 * http://www.iris.edu/manuals/SEEDManual_V2.4.pdf
 * y además del "Norwegian_Technical_18.pdf" disponible en:
 * http://www.sara.pg.it/oth_docs/Norwegian_Technical_18.pdf
 * @author sysop
 *
 */
public class Steim1CW {

	/**
	 * Valor Nibble representable con 2 bits.
	 * Solo Hay 4 posibles valores (0,1,2,3) respectivamente:
	 * NON_DATA, FOUR_1Byte, TWO_2Byte, ONE_4Byte
	 */
	private short tipo;

	private Vector<EcoInt> vEcoInt;
	
	/**
	 * indice del primer EcoInt de este objeto.
	 */
	private int sampleIndex;

	private char[] TXT = null;

	/**
	 * true indica que el Nibble no puede admitir mas datos 
	 */
	private boolean isFull;

	/**
	 * special: wk contains non-data information, such as headers or w0
	 */
	public static final short NON_DATA = 0;//00

	/**
	 * four 1-byte differences contained in wk (four 8-bit samples)
	 */
	public static final short FOUR_1Byte = 1;//01

	/**
	 * two 2-byte differences contained in wk (two 16-bit samples)
	 */
	public static final short TWO_2Byte = 2;//10

	/**
	 * one 4-byte difference contained in wk (one 32-bit sample)
	 */
	public static final short ONE_4Byte = 3;//11

	/**
	 * A Medida que se añaden datos al nibble no es posible que este 
	 * decida cambiar de tipo. Debe conocerse a priori el tipo de nible
	 * que se desea crear. Esto ocurre porque no es trivial conseuir que 
	 * un objeto de esta clase se descomponga en 2 cuando detecte que
	 * para añadir un nuevo dato no puede albergarlo en funcion del espacio disponible.
	 * 
	 * @param tipo
	 */
	public Steim1CW (short tipo, int sampleIndex)
	{
		this.tipo = tipo;
		this.setSampleIndex(sampleIndex);
		this.vEcoInt = new Vector<EcoInt>();
		isFull = false;
	}

	/**
	 * 
	 * @param entero
	 * @return true si el entero se pudo agregar
	 */
	public boolean add(EcoInt ei)
	{
		if (this.isFull) return false;
		boolean respuesta;
		respuesta = true;// Entero ingresado con exito.

		int minBytes;
		minBytes = ei.getMinSteim1Bytes();
		switch (tipo) {
		case FOUR_1Byte:
			if (minBytes==1) { this.vEcoInt.add(ei);}
			if (minBytes==2) { respuesta = false;}
			if (minBytes==4) { respuesta = false;}
			break;
		case TWO_2Byte:
			if (minBytes==1) { this.vEcoInt.add(ei);}
			if (minBytes==2) { this.vEcoInt.add(ei);}
			if (minBytes==4) { respuesta = false;}
			break;
		case ONE_4Byte:
			this.vEcoInt.add(ei);
			break;
		}
		this.refreshIsFull();
		return respuesta;
	}


	/**
	 * Si y solo si el arreglo no es null, y de largo 4 se ingresa el texto
	 * El texto se guardara y desplegara de izquierda a derecha, como
	 * si el caracter de la izquierda fuera el byte mas significativo
	 * @param texto arreglo de 4 caracteres
	 * @return true si el entero se pudo agregar
	 */
	public boolean addTxt(char[] texto)
	{
		if (this.isFull) return false;
		boolean respuesta;
		respuesta = false;// Texto no ingresado.

		if ((texto!=null) && (texto.length==4))
		{
			this.TXT =  texto;
			respuesta = true;// Texto ingresado con exito.
		}
		this.refreshIsFull();
		return respuesta;
	}

	
	@Override
	public String toString() {
		byte[] W;
		W = this.getWord();
		StringBuilder respuesta;
		respuesta = new StringBuilder();
		respuesta.append("Steim1Nibble [tipo=" + tipo + ", isFull=" + isFull + "]");
		if (W!=null)
		{
			for (int i=0;i<4;i++)
			{
				respuesta.append(" ### "+W[i]);
			}
		}
		return respuesta.toString();
	}

	/**
	 * En el caso del NonData,
	 *  Si el arreglo TXT es nulo, retorna un arreglo de 4 bytes con ceros. 
	 * 		Si no,hace un casting de char hacia byte. 
	 * @return WORD = Arreglo de 4 bytes asociado a la logica de este nibble
	 */
	public byte[] getWord ()
	{
		if (!isFull) return null;
		byte[] respuesta;
		respuesta = new byte[4]; // Asi aseguramos que la respuesta es de 4 bytes== 1 WORD
		switch (tipo) {
		case NON_DATA:
			if (TXT==null)
			{
				for (int i=0;i<4;i++) respuesta[i]=0;
			}else
			{
				for (int i=0;i<4;i++)
				{
					respuesta[i] = (byte) TXT[i];
				}
			}
			break;

		case ONE_4Byte:
			byte[] one4Byte;
			one4Byte = new byte[4];			
			one4Byte = vEcoInt.elementAt(0).getByteRepresentation((short)4);
			for (int i=0;i<4;i++)
			{
				respuesta[i] = one4Byte[i];
			}
			break;
		case TWO_2Byte:
			byte[] num1, num2;
			num1 = new byte[2];
			num2 = new byte[2];
			num1 = vEcoInt.elementAt(0).getByteRepresentation((short)2);
			num2 = vEcoInt.elementAt(1).getByteRepresentation((short)2);
			respuesta[0] = num1[0];
			respuesta[1] = num1[1];
			respuesta[2] = num2[0];
			respuesta[3] = num2[1];
			break;
		case FOUR_1Byte:
			for (int i=0;i<4;i++)
			{
				respuesta[i] = vEcoInt.elementAt(i).getByteRepresentation()[0];
			}
			break;
		}
		return respuesta;
	}

	private void refreshIsFull ()
	{
		switch (tipo) {
		case NON_DATA:
			if (TXT==null) 		{isFull = false;	return;}
			if (TXT.length==4) 	{isFull = true;		}
			break;
		case FOUR_1Byte:
			if (vEcoInt.size()==4)	{isFull = true;	}
			break;
		case TWO_2Byte:
			if (vEcoInt.size()==2)	{isFull = true;	}
			break;
		case ONE_4Byte:
			if (vEcoInt.size()==1)	{isFull = true;	}
			break;			
		}
	}


	public short getTipo() {
		return tipo;
	}

	public void setTipo(short tipo) {
		this.tipo = tipo;
	}

	public Vector<EcoInt> getvEcoInt() {
		return vEcoInt;
	}

	public void setvEcoInt(Vector<EcoInt> vEcoInt) {
		this.vEcoInt = vEcoInt;
	}

	public char[] getTXT() {
		return TXT;
	}

	public void setTXT(char[] tXT) {
		TXT = tXT;
	}

	public boolean isFull() {
		return isFull;
	}

	public void setFull(boolean isFull) {
		this.isFull = isFull;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int test;
		test = 1;
		switch (test) {
		case 1:
			pruebasAddTXT();
			break;
		case 2:
			test_1ByteNumbers();
			break;
		case 3:
			test_2ByteNumbers();
			break;

		case 4:
			test_4ByteNumbers();
			break;
		}
	}

	private static void test_4ByteNumbers() {
		System.out.println("test_4ByteNumbers");
		Steim1CW sn;
		boolean addOk;

		System.out.println("Prueba A, añadiendo 2 valores de 4byte");
		sn = new Steim1CW(ONE_4Byte, 0);
		System.out.println(" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(32768));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(327680));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		

		System.out.println("\nPrueba B, añadiendo 1 valor de 4byte y corroborando");
		sn = new Steim1CW(ONE_4Byte, 0);
		EcoInt ei;
		ei = new EcoInt(1213276800);
		System.out.println("ei="+ei.toString());
		sn.add(ei);
		System.out.println("sn="+sn.toString());
		

	}

	private static void test_2ByteNumbers() {
		System.out.println("test_2ByteNumbers");
		Steim1CW sn;
		boolean addOk;

		System.out.println("Prueba A, añadiendo 3 valores de 2byte");
		sn = new Steim1CW(TWO_2Byte, 0);
		System.out.println(" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-200));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(300));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-400));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		
		System.out.println("\nPrueba B, añadiendo 2 valores de 2byte y corroborando");
		sn = new Steim1CW(TWO_2Byte, 0);
		EcoInt ei1, ei2;
		ei1 = new EcoInt(300); ei2 = new EcoInt(-300);
		System.out.println("ei1="+ei1.toString());
		System.out.println("ei2="+ei2.toString());
		sn.add(ei1); sn.add(ei2);
		System.out.println("sn="+sn.toString());

	}

	private static void test_1ByteNumbers() {
		System.out.println("Test2");
		Steim1CW sn;
		boolean addOk;

		System.out.println("Prueba A, añadiendo 5 valores de 1byte");
		sn = new Steim1CW(FOUR_1Byte, 0);
		System.out.println(" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-127));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-1));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(10));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(127));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(0));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		

		System.out.println("\nPrueba B, añadiendo 3 valores de 1byte y luego uno de 4 bytes");
		sn = new Steim1CW(FOUR_1Byte, 0);
		System.out.println(" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-127));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-1));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(10));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(32768));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		

		System.out.println("\nPrueba B1, añadiendo 3 valores de 1byte y luego uno de 4 bytes, luego 1 valor de 1byte");
		sn = new Steim1CW(FOUR_1Byte, 0);
		System.out.println(" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-127));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(-1));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(10));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(32768));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		
		addOk = sn.add(new EcoInt(33));
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());		

		System.out.println("\nPrueba C, añadiendo 4 valores de 1byte y corroborando");
		sn = new Steim1CW(FOUR_1Byte, 0);
		EcoInt ei1, ei2, ei3, ei4;
		ei1 = new EcoInt(-127); 
		ei2 = new EcoInt(-11); 
		ei3 = new EcoInt(30);
		ei4 = new EcoInt(-100);
		System.out.println("ei1="+ei1.toString());
		System.out.println("ei2="+ei2.toString());
		System.out.println("ei3="+ei3.toString());
		System.out.println("ei4="+ei4.toString());
		sn.add(ei1); 
		sn.add(ei2);
		sn.add(ei3); 
		sn.add(ei4);
		System.out.println("sn="+sn.toString());
	}

	/**
	 * Pruebas minimas para evaluar el funcionamiento del método add(char[] )
	 */
	private static void pruebasAddTXT() {
		System.out.println("Test1");
		Steim1CW sn;
		boolean addOk;
		System.out.println("Prueba A, añadiendo dos textos simples al mismo nibble.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(("Hola").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		addOk = sn.addTxt(("CHAO").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());

		System.out.println("Prueba B1, añadiendo null al nibble.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(null);
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());

		System.out.println("Prueba B2, añadiendo null al nibble, luego texto simple.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(null);
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		addOk = sn.addTxt(("Hola").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());

		System.out.println("Prueba C1, añadiendo texto de 3 caracteres.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(("FYI").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		System.out.println("Prueba C2, añadiendo texto de 3 caracteres, luego texto simple.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(("FYI").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		addOk = sn.addTxt(("Hola").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());


		System.out.println("Prueba D, añadiendo texto de 5 caracteres.");
		sn = new Steim1CW(NON_DATA, -1);
		addOk = sn.addTxt(("STEIM").toCharArray());
		System.out.println("addOk="+addOk+" isFull="+sn.isFull()+" tipo="+sn.getTipo());
		
		System.out.println("\nPrueba E, añadiendo texto de 4 caracteres y corroborando");
		sn = new Steim1CW(NON_DATA, -1);
		sn.addTxt(("abcd").toCharArray());
		System.out.println("sn="+sn.toString());
	}

	public void setSampleIndex(int sampleIndex) {
		this.sampleIndex = sampleIndex;
	}

	public int getSampleIndex() {
		return sampleIndex;
	}

}

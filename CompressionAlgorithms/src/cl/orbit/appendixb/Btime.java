/**
 * 
 */
package cl.orbit.appendixb;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Definicion de BTIME
 * Se toma como referencia el "SEEDManual_V2.4.pdf" disponible en:
 * http://www.iris.edu/manuals/SEEDManual_V2.4.pdf
 * Pagina 33, Chapter 3 SEED Conventions.
 * @author sysop
 *
 */
public class Btime {

	private Calendar fecha;

	/**
	 * Valor entre 0 y 9999
	 */
	private int secondFraction;


	public Btime(int secondFraction) {
		super();
		this.fecha = Calendar.getInstance(TimeZone.getTimeZone("GMT"));

		this.secondFraction = secondFraction;
	}

	public byte[] toByteArray()
	{
		byte[] respuesta;
		respuesta = new byte[10];
		byte[] year, dayOfYear, secFrac;
		year = 		Utiles.IntToByteArray(fecha.get(Calendar.YEAR), (short) 2);
		dayOfYear = Utiles.IntToByteArray(fecha.get(Calendar.DAY_OF_YEAR), (short) 2);
		//secFrac = 	Utiles.IntToByteArray(this.secondFraction, (short) 2);
		secFrac = 	Utiles.IntToByteArray ( fecha.get(Calendar.MILLISECOND) * 10
				, (short) 2);
		respuesta[0] =  year[0];
		respuesta[1] =  year[1];
		respuesta[2] =  dayOfYear[0];
		respuesta[3] =  dayOfYear[1];
		respuesta[4] =  (byte) fecha.get(Calendar.HOUR_OF_DAY);
		respuesta[5] =  (byte) fecha.get(Calendar.MINUTE);
		respuesta[6] =  (byte) fecha.get(Calendar.SECOND);//Seconds of day (0—59, 60 for leap seconds)
		respuesta[7] =  (byte) 0;// Unused for data (required for alignment)
		respuesta[8] =  secFrac[0];
		respuesta[9] =  secFrac[1];
		return respuesta;
	}

	public Calendar getFecha() {
		return fecha;
	}

	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}

	/**
	 * Valor entre 0 y 9999
	 * @return 
	 */
	public int getSecondFraction() {
		return secondFraction;
	}

	/**
	 * Valor entre 0 y 9999
	 * @param secondFraction
	 */
	public void setSecondFraction(int secondFraction) {
		this.secondFraction = secondFraction;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}

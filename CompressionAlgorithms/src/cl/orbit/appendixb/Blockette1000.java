/**
 * 
 */
package cl.orbit.appendixb;

/**
 * @author sysop
 *
 */
public class Blockette1000 extends Blockette {

	private byte encodingFormat;
	private byte wordOrder;
	private byte dataRecordLength;
	private byte reserved;
	
	/**
	 * Deprecated.
	 */
	public static final byte WORD_ORDER_LITTLE_ENDIAN = 0;
	public static final byte WORD_ORDER_BIG_ENDIAN = 1;	
	
	public static final byte ENCODING_STEIM1 = (byte) 10;
	public static final byte ENCODING_STEIM2 = (byte) 11;
	
	public Blockette1000 (byte[] offSet, byte encodingFormat, byte dataRecordLength)
	{
		super(Blockette.BLOCKETTE1000,offSet);

		this.encodingFormat = encodingFormat;
		this.wordOrder = WORD_ORDER_BIG_ENDIAN;
		this.dataRecordLength = dataRecordLength;		
	}
	
	/**
	 * 
	 * @return
	 */
	public byte[] toByteArray()
	{
		byte[] respuesta;
		respuesta  = new byte[8];
		respuesta[0] =blocketteCode[0];
		respuesta[1] =blocketteCode[1];
		respuesta[2] =offSet[0];
		respuesta[3] =offSet[1];
		respuesta[4] =encodingFormat;
		respuesta[5] =wordOrder;
		respuesta[6] =dataRecordLength;
		respuesta[7] =reserved;		
		return respuesta;
	}
		
	@Override
	public String toString() {
		return Utiles.toString(this.toByteArray());
	}
	
	
	public static Blockette Steim1_512Bytes(int offst) {
		byte[] offs;
		offs = Utiles.IntToByteArray(offst, (short) 2);
		Blockette respuesta = new Blockette1000(offs, ENCODING_STEIM1, (byte)9);
		return respuesta;
	}

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Blockette b1000;
		b1000 = Steim1_512Bytes(81);
		System.out.println("b1000="+b1000.toString());
	}

}
